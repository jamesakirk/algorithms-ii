import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class SAP {
	private Digraph G;

	// constructor takes a digraph (not necessarily a DAG)
	public SAP(Digraph G) {
		this.G = new Digraph(G);
	}

	// length of shortest ancestral path between v and w; -1 if no such path
	public int length(int v, int w) {
		if (v >= G.V() || v < 0)
			throw new IllegalArgumentException();
		if (w >= G.V() || w < 0)
			throw new IllegalArgumentException();
		int length = sap(v, w)[1];
		return length;
	}

	public int length(Iterable<Integer> A, Iterable<Integer> B) {
		if (Arrays.asList(A).contains(null))
			throw new IllegalArgumentException();
		if (Arrays.asList(B).contains(null))
			throw new IllegalArgumentException();
		if (A == null || B == null)
			throw new IllegalArgumentException();
		ArrayList<Integer> P = new ArrayList<Integer>();
		for (Integer i : A) {
			if (i == null)
				throw new IllegalArgumentException();
			if (i >= G.V() || i < 0)
				throw new IllegalArgumentException();
			P.add(i);
		}

		ArrayList<Integer> Q = new ArrayList<Integer>();
		for (Integer i : B) {
			if (i == null)
				throw new IllegalArgumentException();
			if (i >= G.V() || i < 0)
				throw new IllegalArgumentException();
			Q.add(i);
		}

		return sap(P, Q)[1];
	}

	// a common ancestor of v and w that participates in a shortest ancestral path;
	// -1 if no such path
	public int ancestor(int v, int w) {
		// if(v == null || w == null) throw new IllegalArgumentException();
		if (v >= G.V() || v < 0)
			throw new IllegalArgumentException();
		if (w >= G.V() || w < 0)
			throw new IllegalArgumentException();
		return sap(v, w)[0];
	}

	public int ancestor(Iterable<Integer> A, Iterable<Integer> B) {
		if (A == null || B == null) throw new IllegalArgumentException();
		ArrayList<Integer> P = new ArrayList<Integer>();
		for (Integer i : A) {
			if (i == null) throw new IllegalArgumentException();
			if (i >= G.V() || i < 0) throw new IllegalArgumentException();
			P.add(i);
		}

		ArrayList<Integer> Q = new ArrayList<Integer>();
		for (Integer i : B) {
			if (i == null) throw new IllegalArgumentException();
			if (i >= G.V() || i < 0) throw new IllegalArgumentException();
			Q.add(i);
		}

		return sap(P, Q)[0];
	}

	private int[] sap(int v, int w) {
		ArrayList<Integer> V = new ArrayList<Integer>();
		ArrayList<Integer> W = new ArrayList<Integer>();
		V.add(v);
		W.add(w);
		return sap(V, W);
	}

	private int[] sap(ArrayList<Integer> A, ArrayList<Integer> B) {
		int shortest = Integer.MAX_VALUE;
		int ancestor = -1;
		int size = G.V();

		// implementation tradeoff: using int, not null so need to initialize to
		// negative
		int[] sourceA = new int[size];
		int[] sourceB = new int[size];
		int[] distA = new int[size];
		int[] distB = new int[size];
		for (int i = 0; i < size; i++) {
			sourceA[i] = -1;
			sourceB[i] = -1;
			distA[i] = -1;
			distB[i] = -1;
		}
		foo cur = new foo(A, B, 0);
		foo next = new foo();

		while (shortest > cur.dist && (!cur.QA.isEmpty() || !cur.QB.isEmpty())) {
			for (int i : cur.QA) {
				if (distA[i] >= 0)
					continue;
				else {
					distA[i] = cur.dist;
					if (distB[i] >= 0) {
						// i is a common ancestor
						if (shortest > distA[i] + distB[i]) {
							shortest = distA[i] + distB[i];
							ancestor = i;
						}

					}
					Iterable<Integer> d = G.adj(i);
					for (int j : d)
						next.QA.add(j); // iff not marked, add to queue

				} // ifelse
			} // for
			for (int i : cur.QB) {
				if (distB[i] >= 0)
					continue;
				else {
					distB[i] = cur.dist;
					if (distA[i] >= 0) {
						// i is a common ancestor
						if (shortest > distA[i] + distB[i]) {
							shortest = distA[i] + distB[i];
							ancestor = i;
						}
					}
					for (int j : G.adj(i))
						next.QB.add(j); // iff not marked, add to queue

				} // ifelse
			} // for
			next.dist = cur.dist + 1;
			cur = next;
			next = new foo();
		} // while
		if (ancestor == -1)
			shortest = -1;
		int[] result = { ancestor, shortest };
		return result;
	} // method

	private class foo {
		private Deque<Integer> QA = new LinkedList<Integer>();
		private Deque<Integer> QB = new LinkedList<Integer>();
		private int dist;

		foo() {
			dist = 0;
		}

		foo(ArrayList<Integer> QA, ArrayList<Integer> QB, int dist) {
			this.QA.addAll(QA);
			this.QB.addAll(QB);
			this.dist = dist;
		}

		public String toString() {
			return "dist:" + dist + " " + QA.toString() + " " + QB.toString();
		}

	}

	// do unit testing of this class
	public static void main(String[] args) {
		String hypernymsFile = args[0];
		if (hypernymsFile == null) throw new IllegalArgumentException();
		Digraph D = new Digraph(new In(hypernymsFile));
		SAP sap = new SAP(D);

		while (!StdIn.isEmpty()) {
			int v = StdIn.readInt();
			int w = StdIn.readInt();
			int length = sap.length(v, w);
			int ancestor = sap.ancestor(v, w);
			StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
		}
	}
}