import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Digraph;

public class WordNet {
	private HashMap<Integer, ArrayList<String>> nounsOf = new HashMap<Integer, ArrayList<String>>();
	private HashMap<String, ArrayList<Integer>> indicesOf = new HashMap<String, ArrayList<Integer>>();
	private Digraph D;
	private SAP sap;
	public WordNet(String synsetsFile, String hypernymsFile) {
		if (synsetsFile == null || hypernymsFile == null) throw new IllegalArgumentException();

		Set<Integer> vertexSet = new HashSet<Integer>();
		Set<Integer> sourceSet = new HashSet<Integer>();
		Set<Integer> differenceSet;

		ArrayList<String> nouns;
		In in = new In(synsetsFile);
		String line;

		//Read in the first file and populate vertexSet and sourceSet dictionaries
		while (!in.isEmpty()) {
			line = in.readLine();
			String[] parts = line.split(",");
			int id = Integer.parseInt(parts[0]);
			String synet = parts[1];
			nouns = new ArrayList<String>();
			for (String s : synet.split(" ")) {
				nouns.add(s);
			}
			nounsOf.put(id, nouns); // now, ArrayList nouns should contain all of the nouns for each id

			// next, update the noun-to-int map
			for (String s : nouns) {
				ArrayList<Integer> al = indicesOf.get(s); // fetch
				if (al == null)
					al = new ArrayList<Integer>();
				al.add(id); // add
				indicesOf.put(s, al); // update
			}
		}
		
		D = new Digraph(nounsOf.keySet().size());

		//read in edges and add them to the Digraph
		in = new In(hypernymsFile);
		while (!in.isEmpty()) {
			line = in.readLine();
			String[] vertices = line.split(",");
			int s = Integer.parseInt(vertices[0]);
			if(vertices.length > 1) {
				sourceSet.add(s); //only add the source if there is a target.
			}
			for (int i = 1; i < vertices.length ; i++) {
				int t = Integer.parseInt(vertices[i]);
				D.addEdge(s, t);
			}
		}

		//locate vertices that are not sources. these are the roots.
		vertexSet.addAll(nounsOf.keySet());
		differenceSet = new HashSet<Integer>(vertexSet);
		differenceSet.removeAll(sourceSet);
		
		//throw exception if there is not exactly one root.
		if (differenceSet.size() != 1) throw new IllegalArgumentException();
		
		sap = new SAP(D);
	}

	public Iterable<String> nouns() {
		return indicesOf.keySet();
	}

	public boolean isNoun(String word) {
		if (word == null) throw new IllegalArgumentException();
		return indicesOf.containsKey(word);
	}

	public String sap(String nounA, String nounB) {
		if(nounA == null || nounB == null) throw new IllegalArgumentException();
		if(!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException();
		int ancestor = sap.ancestor(indicesOf.get(nounA), indicesOf.get(nounB));
		String s = new String();
		for (String t : nounsOf.get(ancestor)) {
			s += t + " ";
		}
		return s;
	}
	public int distance(String nounA, String nounB) {
		if(nounA == null || nounB == null) throw new IllegalArgumentException();
		if(!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException();
		return sap.length(indicesOf.get(nounA), indicesOf.get(nounB));
	}	


	public static void main(String[] args) {
		WordNet W = new WordNet(args[0], args[1]);
		// now we can play back nounsOf
		String[] testWords = {"m", "cactusbreath", "factor"};
		for(String s : testWords) 
			System.out.println("Is " + s + " a noun? " + W.isNoun(s));
	
		System.out.print("There are ");
		int x = 0;
		for (String s : W.nouns())
		{
			x++;
		}
		System.out.print(x + " nouns\n");
		
		String[] sourceA = {"increase", "demotion", "cactus", "flower"};
		String[] sourceB = {"hope", "god", "dinosaur", "zoo"};
		for(int i = 0; i < sourceA.length; i++) {
			System.out.println("What is the common ancestor of " + sourceA[i] + " and " + sourceB[i] +" ?");
			String Ancestor = W.sap(sourceA[i] , sourceB[i]);
			System.out.println("Ancestor: " + Ancestor);
			System.out.println("The distance is: " + W.distance(sourceA[i], sourceB[i]));
		}

	}
 }
