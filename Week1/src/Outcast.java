import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {
	private WordNet wn;
   public Outcast(WordNet wordnet){
	   wn = wordnet;
   }
   // constructor takes a WordNet object
   public String outcast(String[] nouns) {   // given an array of WordNet nouns, return an outcast
	   int max = -1;
	   int blacksheep = 0;
	   for(int i = 0 ; i < nouns.length; i++) {
		   int[] diffs = new int[nouns.length];
		   for(int j = 0; j < nouns.length; j++) {
			   diffs[j] = wn.distance(nouns[i], nouns[j]);
		   }
		    int sum = 0;
		    for (int value : diffs) {
		        sum += value;
		    }
		    if (sum > max) {
		     blacksheep = i;
		     max = sum;
		    }
	   }
		return nouns[blacksheep];
   }
   public static void main(String[] args) {
	    WordNet wordnet = new WordNet(args[0], args[1]);
	    Outcast outcast = new Outcast(wordnet);
	    for (int t = 2; t < args.length; t++) {
	        In in = new In(args[t]);
	        String[] nouns = in.readAllStrings();
	        StdOut.println(args[t] + ": " + outcast.outcast(nouns));
	    }
	}
}