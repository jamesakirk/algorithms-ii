import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Digraph;

public class Reader1_TestA {

	public Reader1_TestA() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws IOException  {
		// TODO Auto-generated method stub
		
		HashMap<Integer,ArrayList<String>> nounsOf   = new HashMap<Integer,ArrayList<String>>();
		HashMap<String,ArrayList<Integer>> indicesOf = new HashMap<String,ArrayList<Integer>>();
		ArrayList<String> nouns;
		
	    In in = new In(args[0]);
	    String line;
	    while (!in.isEmpty()) {
	        //Populate first hashmap
	        line = in.readLine();
	        String[] parts = line.split(",");
	        int id = Integer.parseInt(parts[0]);
	        String synet = parts[1];
	        nouns = new ArrayList<String>();
	        for(String s : synet.split(" ")) {
	        	nouns.add(s);
 	        }

	        nounsOf.put(id, nouns);
	        //now, nouns arraylist should contain all of the nouns for id
	        
	        //next, update the noun > int map
	        for(String s: nouns) {
	        	ArrayList<Integer> al = indicesOf.get(s); //fetch
	        	if(al == null) al = new ArrayList<Integer>();
	        	al.add(id); //add
	        	indicesOf.put(s, al); //update
	        }
		}
	    //now we can play back nounsOf
	    //for(int i : nounsOf.keySet())
	    	//System.out.println("i: " + i + " nouns: " + nounsOf.get(i));    
	    //now we can play back indicesOf:
	    //for(String s : indicesOf.keySet())
	    	//System.out.println("s: " + s + " id:" + indicesOf.get(s));
	    
	    
	    Digraph D = new Digraph(nounsOf.keySet().size());
	    
		Set<Integer> vertexSet = new HashSet<Integer>();
		vertexSet.addAll(nounsOf.keySet());
		Set<Integer> sourceSet = new HashSet<Integer>();


		

		//		ArrayList<String> records = new ArrayList<String>();
		//		try (Scanner scanner = new Scanner(args[1]);) {
		//		    while (scanner.hasNextLine()) {
		//		        records.add(getRecordFromLine(scanner.nextLine()));
		//		    }
		//		}

		{	
			BufferedReader reader 
			= new BufferedReader(new FileReader(args[1]));

			String result = reader.readLine();
			result = reader.readLine();
			result = reader.readLine();
			in = new In(args[1]);
			while (!in.isEmpty()) {

				line = in.readLine();
				System.out.println("read a line " + line);
				//line = in.readLine();
				//System.out.println("read another line " + line);
				String[] vertices = line.split(",");
				//        Arrays.Utils.reverse(vertices);
				int s = Integer.parseInt(vertices[0]);
				sourceSet.add(s);
				for(int i = vertices.length - 2; i>= 0; i--) {   	
					int t = Integer.parseInt(vertices[i]);
					D.addEdge(s, t);
				}
			}
		}
		Set<Integer> differenceSet = new HashSet<Integer>(vertexSet);
		differenceSet.removeAll(sourceSet);
		System.out.print("The roots are: ");
		for (int i : differenceSet) {
			System.out.print(i + " ");
		}
	}
}

