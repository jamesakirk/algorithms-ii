import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;

import java.util.ArrayList;
import java.util.HashMap;

/*
public BaseballElimination(String filename)                    // create a baseball division from given filename in format specified below
public              int numberOfTeams()                        // number of teams
public Iterable<String> teams()                                // all teams
public              int wins(String team)                      // number of wins for given team
public              int losses(String team)                    // number of losses for given team
public              int remaining(String team)                 // number of remaining games for given team
public              int against(String team1, String team2)    // number of remaining games between team1 and team2
public          boolean isEliminated(String team)              // is given team eliminated?
public Iterable<String> certificateOfElimination(String team)  // subset R of teams that eliminates given team; null if not eliminated
 */
public class BaseballElimination {

	private HashMap<String, Integer> nameToNumber; // given team name, what is the team number?
	private String[] numberToName; // given team number, what is the number of the team?
	private int[] wins;
	private int[] losses;
	private int[] remaining;
	private int[][] schedule;
	private int n;

	private void validateTeam(String team) {
		if (!nameToNumber.containsKey(team))
			throw new IllegalArgumentException(team + " is not a valid team.");
	}

	public BaseballElimination(String filename) {
		In in = new In(filename);
		n = in.readInt(); // read in the number of teams
		nameToNumber = new HashMap<String, Integer>();
		numberToName = new String[n];
		wins = new int[n];
		losses = new int[n];
		remaining = new int[n];
		schedule = new int[n][n];
		for (int j = 0; j < n; j++) {
			String teamName = in.readString();
			nameToNumber.put(teamName, j);
			numberToName[j] = teamName;
			wins[j] = in.readInt();
			losses[j] = in.readInt();
			remaining[j] = in.readInt();
			for (int i = 0; i < n; i++)
				schedule[i][j] = in.readInt();
		}
	}

	public int numberOfTeams() {
		return n;
	}

	public Iterable<String> teams() {
		ArrayList<String> teamNames = new ArrayList<String>();
		for (int i = 0; i < n; i++) {
			teamNames.add(numberToName[i]);
		}
		return teamNames;
	}

	public int wins(String team) {
		validateTeam(team);
		return wins[nameToNumber.get(team)];
	}

	public int losses(String team) {
		validateTeam(team);
		return losses[nameToNumber.get(team)];
	}

	public int remaining(String team) {
		validateTeam(team);
		return remaining[nameToNumber.get(team)];
	}

	public int against(String p, String q) {
		validateTeam(p);
		validateTeam(q);
		return schedule[nameToNumber.get(p)][nameToNumber.get(q)];
	}

	public boolean isEliminated(String team) {
		validateTeam(team);
		Iterable<String> certs = certificateOfElimination(team);
		if (certs == null)
			return false;
		else
			return true;
	}

	public Iterable<String> certificateOfElimination(String team) {
		validateTeam(team);
		ArrayList<String> killers = new ArrayList<String>();
		int teamIndex = nameToNumber.get(team);
		boolean killed = false;
		// check for trivial elimination:
		for (int i = 0; i < n; i++) {
			if (wins[teamIndex] + remaining[teamIndex] < wins[i]) {
				killed = true;
				killers.add(numberToName[i]);
			}
		}
		if (killed) {
			return killers;
		}

		// vertex partitioning schema:
		// s : 0
		// t : 1
		// Gi : 2+i n games i=0...n-1
		// Tj : n+1+j m teams j=0...m-1
		int source = 0;
		int target = 1;
		int m = n * (n - 1) / 2;
		int firstGame = 2; // G_0
		int firstTeam = 2 + m; // T_0
		int lastTeam = 1 + m + n; // T_M-1
		FlowNetwork FN = new FlowNetwork(2 + m + n);

		int gameVertex = firstGame;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				int rounds = schedule[i][j];
				if (rounds > 0) {
					FN.addEdge(new FlowEdge(source, gameVertex, rounds));
					FN.addEdge(new FlowEdge(gameVertex, i + m + 2, Double.POSITIVE_INFINITY));
					FN.addEdge(new FlowEdge(gameVertex, j + m + 2, Double.POSITIVE_INFINITY));
				}
				gameVertex++;
			}
		}

		// add edges from teams to target
		for (int currentTeamVertex = firstTeam; currentTeamVertex <= lastTeam; currentTeamVertex++) {
			int currentTeamNumber = currentTeamVertex - firstTeam;
			int excludedTeamNumber = nameToNumber.get(team);
			int Wx = wins[excludedTeamNumber];
			int Rx = remaining[excludedTeamNumber];
			int Ri = wins[currentTeamNumber];
			int weight = Wx + Rx - Ri;
			if (weight < 0)
				killers.add(numberToName[currentTeamNumber]);
			else
				FN.addEdge(new FlowEdge(currentTeamVertex, target, (double) weight));
		}
		// calculate max flow
		FordFulkerson FF = new FordFulkerson(FN, 0, 1);
		double sourceCapacity = 0;
		for(FlowEdge e : FN.adj(source)) {
			sourceCapacity += e.capacity();
		}
		if (FF.value() < sourceCapacity) {
			killed = true;
			// s -> game edges are NOT saturated
			// so team x is eliminated
			// observation: if a team is in the mincut, then it contributed to x's defeat
			// and should be included in the list
			for (int i = firstTeam; i <= lastTeam; i++) {
				if (FF.inCut(i))
					killers.add(numberToName[i - firstTeam]);
			}
		}
		if (killed)
			return killers;
		else
			return null;
	}

	public static void main(String[] args) {
		BaseballElimination division = new BaseballElimination(args[0]);
		for (String team : division.teams()) {
			if (division.isEliminated(team)) {
				StdOut.print(team + " is eliminated by the subset R = { ");
				for (String t : division.certificateOfElimination(team)) {
					StdOut.print(t + " ");
				}
				StdOut.println("}");
			} else {
				StdOut.println(team + " is not eliminated");
			}
		}
	}
}
