import java.io.File;

import edu.princeton.cs.algs4.Picture;

public class SeamCarver {
	private Picture pic;
	private boolean vertical = false;
	public SeamCarver(Picture picture) {
		if (picture == null)
			throw new IllegalArgumentException("Constructor failure: argument is not a Picture");
		this.pic = new Picture(picture); // defensive copy
	}

	public Picture picture() {
		return new Picture(pic); // return defensive copy
	}

	public int width() {
		return pic.width();
	}

	public int height() {
		return pic.height();
	}
	public double energy(int x, int y) {
		if (x > pic.width() - 1 || x < 0 || y > pic.height() - 1 || y < 0) {
			throw new IllegalArgumentException("Arguments to energy are out-of-bounds");
		}

		if (x == pic.width() - 1 || x == 0 || y == pic.height() - 1 || y == 0) {
			return 1000.0;
		} else {
			int x_c1 = pic.getRGB(x - 1, y);
			int x_c2 = pic.getRGB(x + 1, y);
			int y_c1 = pic.getRGB(x, y - 1);
			int y_c2 = pic.getRGB(x, y + 1);
			// this is really dirty and should be cleaned up like X_(C1 >> 16) & 0xFF
			int dxr = Math.abs(((x_c1 & 0xff0000)) - ((x_c2 & 0xff0000))) >> 16;
			int dxg = Math.abs(((x_c1 & 0x00ff00)) - ((x_c2 & 0x00ff00))) >> 8;
			int dxb = Math.abs(((x_c1 & 0x0000ff)) - ((x_c2 & 0x0000ff))) >> 0;
			int dyr = Math.abs(((y_c1 & 0xff0000)) - ((y_c2 & 0xff0000))) >> 16;
			int dyg = Math.abs(((y_c1 & 0x00ff00)) - ((y_c2 & 0x00ff00))) >> 8;
			int dyb = Math.abs(((y_c1 & 0x0000ff)) - ((y_c2 & 0x0000ff))) >> 0;

			int dxsq = dxr * dxr + dxg * dxg + dxb * dxb;
			int dysq = dyr * dyr + dyg * dyg + dyb * dyb;
			int sum = dxsq + dysq;
			double e = Math.sqrt(sum);
			return e;
		}
	}

	public int[] findHorizontalSeam() {
		vertical = false;
		return findSeam();
	}

	public int[] findVerticalSeam() {
		vertical = true;
		return findSeam();
	}

	private int[] findSeam() {
		int width = pic.width();
		int height = pic.height();
		if (!vertical) {
			width = pic.height();
			height = pic.width();
		}
		double[][] energies = new double[width][height];
		double[][] pathEnergy = new double[width][height];
		int[] seam = new int[height];
		// ok, now let's build a local cache of energies:

		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				energies[i][j] = (vertical) ? energy(i, j) : energy(j, i);
			}
		}

		// now, calculate the paths:
		for (int j = height - 1; j >= 0; j--) {
			for (int i = 0; i < pathEnergy.length; i++) {
				if (j == height - 1) { 
					// nochildren
					pathEnergy[i][j] = energies[i][j]; 
				}
				else {	
					// children exist
					double minChildEnergy;
					minChildEnergy = pathEnergy[i][j + 1]; // initialize to middle child
					if (i != pathEnergy.length - 1) {
						// if not right edge, check the right child
						if (pathEnergy[i + 1][j + 1] < minChildEnergy) {
							// right child is smallest
							minChildEnergy = pathEnergy[i + 1][j + 1];
						}
					}
					if (i != 0) {
						// if not left edge, check the left child
						if (pathEnergy[i - 1][j + 1] < minChildEnergy) {
							// leftchild is smallest
							minChildEnergy = pathEnergy[i - 1][j + 1];
						}
					}
					pathEnergy[i][j] = minChildEnergy + energies[i][j];
				}
			}
		}

		// First, find the smallest element in the top row. This can be anywere.
		double minVal = Double.MAX_VALUE;
		for (int i = 0; i < pathEnergy.length; i++) {
			double propVal = pathEnergy[i][0];
			if (Double.compare(propVal, minVal) < 0) {
				minVal = pathEnergy[i][0];
				seam[0] = i;
			}
		}
		int parent = seam[0];
		// next, for each node, we look at the left, middle, and right children for that
		// node
		// and choose the min
		for (int j = 1; j < height; j++) {

			// by default choose middle child
			minVal = pathEnergy[parent][j];
			int minX = parent;

			if (!(parent == 0)) {
				// check the Left child
				if (Double.compare(pathEnergy[parent - 1][j], minVal) < 0) {
					minVal = pathEnergy[parent - 1][j];
					minX = parent - 1;
				}
			}
			if (!(parent == pathEnergy.length - 1)) {
				// check the Right child
				if (Double.compare(pathEnergy[parent + 1][j], minVal) < 0) {
					minVal = pathEnergy[parent + 1][j];
					minX = parent + 1;
				}
			}
			seam[j] = minX;
			parent = minX;
			if (j == 1)
				seam[0] = seam[1]; // on first path, adjust parent to align with this
		}
		return seam;
	}

	public void removeVerticalSeam(int[] seam) {
		validateSeam(seam, pic.height());
		Picture newPicture = new Picture(pic.width() - 1, pic.height());
		for (int j = 0; j < pic.height(); j++) {
			for (int i = 0; i < seam[j]; i++) {
				newPicture.setRGB(i, j, pic.getRGB(i, j));
			}
			for (int i = seam[j]; i < newPicture.width(); i++) {
				newPicture.setRGB(i, j, pic.getRGB(i + 1, j));
			}
		}
		this.pic = newPicture;
	}

	public void removeHorizontalSeam(int[] seam) {
		validateSeam(seam, pic.width());
		Picture newPicture = new Picture(pic.width(), pic.height() - 1);
		for (int i = 0; i < pic.width(); i++) {
			for (int j = 0; j < seam[i]; j++) {
				newPicture.setRGB(i, j, pic.getRGB(i, j));
			}
			for (int j = seam[i]; j < newPicture.height(); j++) {
				newPicture.setRGB(i, j, pic.getRGB(i, j + 1));
			}
		}
		this.pic = newPicture;
	}

	private void validateSeam(int[] seam, int dimension) {
		if (seam == null)
			throw new IllegalArgumentException("Argument seam is null");
		if (seam.length != dimension)
			throw new IllegalArgumentException("Argument seam does not match width");
		for (int i = 0; i < seam.length - 1; i++) {
			int diff = Math.abs(seam[i] - seam[i + 1]);
			if (diff > 1)
				throw new IllegalArgumentException("Argument seam jumps by more than 1 per element");
		}
	}

	// unit testing (optional)
	public static void main(String[] args) {
		File fileA = new File("/home/lnxusr/Algorithms/algorithms-ii/Week2/spec/seam/6x5.png");
		SeamCarver A = new SeamCarver(new Picture(fileA));
		int[] vSeam = A.findVerticalSeam();
		A.removeVerticalSeam(vSeam);
		int[] hSeam = A.findHorizontalSeam();
		A.removeHorizontalSeam(hSeam);
		// System.out.println(fileA.getName());
		// A.test();
		// A.picture().show();
	}

}