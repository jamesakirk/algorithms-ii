import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.Alphabet;
public class MoveToFront {
	// private static Alphabet alphabet = Alphabet.UPPERCASE;
	// private static Alphabet alphabet = Alphabet.UPPERCASE;
	private static Alphabet alphabet = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	private static int R = alphabet.radix();
	private static int[] mtfIndices = new int[R];
	private static boolean debug = true;
	private MoveToFront() {
	}
	// apply move-to-front encoding, reading from standard input and writing to
	// standard output
	public static void encode()
	{
		for(int i = 0; i < R; i++)
			mtfIndices[i] = alphabet.toChar(i);
		while(!BinaryStdIn.isEmpty()) {
			char c = BinaryStdIn.readChar();
			if(debug) System.out.print(c);
			int i = find(c);			
			BinaryStdOut.write(i); //does nothing????
			if(i >= 0) moveFront(i);
		}
        BinaryStdOut.flush();
		BinaryStdIn.close();
	}
	private static void moveFront(int i) {
		if (debug) System.out.print(" -> " + i + "\n");
	}
	private static int find(char c) {
		for(int i = 0; i < R; i++) {
			if(mtfIndices[i] == c)
				return i;
		}
		return -1;
	}

	// apply move-to-front decoding, reading from standard input and writing to
	// standard output
	// public static void decode()

	// if args[0] is "-", apply move-to-front encoding
	// if args[0] is "+", apply move-to-front decoding
	public static void main(String[] args) {
		//String[] args = new String[1];
		//bargs[0] = "-";
		switch (args[0]) {
		case "-": 
			encode();// encode
			break;
		case "+": // decode
			break;
		default:
			break;
		}
	}
} 