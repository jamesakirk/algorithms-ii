/******************************************************************************
 * Compilation: javac CircularSuffixArray.java Execution: java
 * CircularSuffixArray Dependencies: Data files:
 *
 * CircularSuffixArray
 *
 * - Given a String, create a a circular suffix array (CSA) from that string,
 * 
 * - Sort the suffix array
 * 
 * - Output is the index of the original string, 
 * 	 and the last column of each suffix
 *
 * Uses extra space proportional TODO
 *
 * % java CircularSuffixArray TODO
 *
 ******************************************************************************/
public class CircularSuffixArray {
	private final int debug = 0;  // print additional diagnostics
								  // debug level 1 prints out CSAs
								  // debug level 2 prints out cumulates
	private final String masterString; // this will form the basis of the CSA
	private final int N;    // length of masterStrin the NxN CSA
	private final static int R = 256;  // radix 
	private int[] sortedOffsets;  // the ints are the original indices 
							      // sortedOffsets[0] is the first row in the current CSA
								  // sortedOffsets[i] == 0 is the first row of the unsorted CSA
								  // if sortedOffsets[i] == 0 , then i is the location of the initial string.
	private int[] locateOffset;   // the ints are the current locations
							      // if locateOffset[0] == i, then i is the location of the initial string
	
	/**
	 * Creates and sorts a Circular Suffix Array CSA
	 *
	 * @param s the String to permute
	 */
	public CircularSuffixArray(String s) {
		if (s == null)
			throw new java.lang.IllegalArgumentException();
		this.masterString = s;
		this.N = s.length();
		if (debug > 0)
			System.out.print("Before Sorting: \n" + originalToString());
		sortedOffsets = new int[N];
		locateOffset = new int[N];
		for(int i = 0; i < N; i++) {
			sortedOffsets[i] = i;
		}
		// sort the offset (on basis of chars in string)
		sortedOffsets = lsdSort(sortedOffsets);
		for(int i = 0; i < N; i++) {
			locateOffset[sortedOffsets[i]] = i;
		}
		
		if(debug > 0) System.out.print("After Sorting: \n" + ToString(sortedOffsets));
		if(debug > 0) {
			System.out.print("Final Offsets a.k.a index[i]: [ ");
			for(int i = 0; i < N; i++) 
				System.out.print(sortedOffsets[i] + " ");
			System.out.print("] \n");
		}
	}
	
	/**
	 * Return the size of our CSA
	 */
	public int length() { return N; }
	
	/**
	 * Finds the current index of row in the CSA given it's original index.
	 *
	 * @param i the original index of the row
	 * 
	 * @return the current index of the row
	 *         
	 * @throws IllegalArgumentException unless {@code 0 < i || i > N-1}
	 */
	public int index(int i) {
		if(i<0 || i > N-1) throw new java.lang.IllegalArgumentException();
		return sortedOffsets[i];
	}
	
	/**
     * Uses LSD radix sort to sort the offset indices on the basis of the associated suffixes
	 *
	 * @param a[] the original indexes of the rows
	 * 
	 * @return int[] the new indexes of the rows
	 */
	private int[] lsdSort(int[] a) {
		for(int d = a.length-1; d >= 0; d--) {
			sortOnColumn(a, d);
		}
		return a;
	}
	
	/**
	 * Sort an array of offset indices on the basis of a specific column in the CSA
	 *
	 * @param a[] the indices for our current suffixes
	 * 
	 * @return int[] the new indexes of the rows
	 */
	private void sortOnColumn(int[] a, int d) {
		if(debug > 0) System.out.println("Before sorting on column " + d +", suffixes are: ");
		if(debug > 0) System.out.print(ToString(a));
		if(debug > 0) {
			System.out.print("offsets = [");
			for(int i = 0; i < a.length; i++) {
				System.out.print(a[i] + " ");
			}
			System.out.print("]\n");
		}
		StringBuilder slice = new StringBuilder();
		int[] aux = new int[a.length];
		
		//  step 1: count the frequencies
		int cumulates[] = new int[R + 1];
		for(int i = 0; i < a.length; i++) {
			char c = getChar(a[i], d);
			slice.append(c);
			cumulates[ c + 1]++;
		}	
		if(debug > 0) System.out.println("Sorting along slice:");
		if(debug > 0) System.out.println(slice.toString());
		if(debug > 0) System.out.print(' ');
		
		//  step 2: calculate the cumulates
		for(int r = 0; r < R; r++)
			cumulates[r+1] += cumulates[r];

		if(debug > 1) {
			System.out.println("Cumulates calculated:");
			print(cumulates);
		}
		if (debug > 0) slice = new StringBuilder();
		
		//  step 3: copy into aux array while updating cumulates
		for(int i = 0; i < a.length; i++) {
			char curChar = getChar(a[i], d);
			if(debug > 0) slice.append(curChar);
			int thisIndex = cumulates[curChar];
			aux[thisIndex] = a[i];
			cumulates[curChar]++ ;
		}
		if(debug > 1) {
			System.out.println("Traversed slice: " + slice.toString());
			System.out.println("After aux population");
			print(cumulates);
		}
		
		// 	step 4: copy back into original array, and update inverse offsets
		for(int i = 0; i < a.length; i++)
			a[i] = aux[i];
		for(int i = 0; i < a.length; i++)
			locateOffset[a[i]] = i; // update inverse offset
		if(debug > 0)
		{
			System.out.println("After sorting on column " + d);
			System.out.print("offsets = [");
			for(int i = 0; i < a.length; i++) {
				System.out.print(a[i] + " ");
			}
			System.out.print("]\n");
			System.out.print("Circular suffixes are: \n");
			System.out.print(ToString(a));
		}
	}

	/**
	 * Returns the character located at a specific suffix and digit digit in this CSA
	 *
	 * @param row the current index of row in the CSA to extract
	 * 
	 * @param col the digit of the suffix to extract
	 *
	 * @return char the character at that location
	 */
	private char getChar(int row, int col) {
		int idx = (N+col+row) % N;
		char c = masterString.charAt(idx);
		return c;
	}
	
	/**
	 * Creates and returns a String representation of a CSA in given state
	 *
	 * @param a[] the indices of the original locations of the suffixes
	 *
	 * @return string representation of a CSA state
	 */		
	private String ToString(int[] a) {
		StringBuilder sb = new StringBuilder();
		sb.append(N);
		sb.append('\n');
		for(int row = 0; row < N; row++)
			sb.append(" " + row);
		sb.append("\n ");
		for(int row = 0; row < N; row++)
			sb.append("--");
		for(int row = 0; row < N; row++) {
			sb.append('\n');
			for (int col = 0; col < N; col++) {
				sb.append(' ');
				sb.append(getChar(a[row],col));
			}
			sb.append(" | " + a[row] );
		}
		sb.append('\n');
		return sb.toString();
	}
	
	/**
	 * Creates and returns a String representation of the CSA in it's original state
	 *
	 * @return string representation of the CSA in it's original state
	 */		
	private String originalToString() {
		int[] a = new int[N];
		for(int i = 0 ; i < N; i++)
			a[i] = i;
		return ToString(a);
	}

	/**
	 * Prints the the state of a cumulates array over the span of uppercase letters
	 *
	 * @param a[] the cumulates array
	 */	
	private void print(int[] a) {
		System.out.println("c | id | val");
		for(int i = 65; i < 91; i++) {
			System.out.println((char) i + " | " + i + " | " + a[i]);
		}
	}
	
	/**
	 * Performs unit testing of CSA
	 */	
	public static void main(String[] args) {
		String s = "BABBBBBBBBABAABBBAAABAAABBAAABBBABBABAABBAAAAAAAAA";
		CircularSuffixArray CSA = new CircularSuffixArray(s);
		int n = CSA.length();
		
		int[] indices = new int[n];
		for(int i = 0; i < n; i++) {
			indices[CSA.index(i)] = i;
		}
		
		System.out.print("** Results **\nindex[i]: [ ");
		for(int i = 0; i < n; i++) 
			System.out.print(CSA.index(i) + " ");
		System.out.print("] \n");
		
		//int idx = (N+col+row) % N;
		//char c = masterString.charAt(idx);
	
		StringBuilder sbCSA = new StringBuilder();
		sbCSA.append('\n');
		for(int row = 0; row < n; row++)
			sbCSA.append(" " + row);
		sbCSA.append("\n ");
		for(int row = 0; row < n; row++)
			sbCSA.append("--");
		for(int row = 0; row < n; row++) {
			sbCSA.append('\n');
			for (int col = 0; col < n; col++) {
				sbCSA.append(' ');
				sbCSA.append( s.charAt((n + CSA.index(row) + col) %n));
			}
			sbCSA.append(" | " + CSA.index(row) );
		}
		System.out.println(sbCSA.toString());
		
		
		StringBuilder result = new StringBuilder();
		for(int row = 0; row < n; row++) 
			result.append(
					s.charAt(
							(CSA.index(row) + n - 1)%n));
		System.out.println("Output: \n" + CSA.index(0));
		System.out.println(result.toString());
		
	}    // main
}    // class

