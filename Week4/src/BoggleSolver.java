import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.SET;
//import edu.princeton.cs.algs4.TrieST;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BoggleSolver
{
	private StringBuilder prefix = new StringBuilder();
	private int rows;
	private int cols;
	private int N;
	private char[] board;
	private boolean[] hasUp;
	private boolean[] hasDown;
	private boolean[] hasRight;
	private boolean[] hasLeft;

	//TST will be more memory efficient than R-Trie, but is not as fast. Speed is KEY with this assignment.
	private TrieJK<Boolean> ST = new TrieJK<Boolean>();
	// Initializes the data structure using the given array of strings as the dictionary.
	// (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
	public BoggleSolver(String[] dictionary)
	{
		for(int i = 0; i < dictionary.length; i++) {
			ST.put(dictionary[i]);
		}
		//testScoring();
	}

	private void initilizeBoard(BoggleBoard board) {
		this.rows = board.rows();
		this.cols = board.cols();
		this.N = this.cols * this.rows;
		this.hasUp    = new boolean[N];
		this.hasDown  = new boolean[N];
		this.hasRight = new boolean[N];
		this.hasLeft  = new boolean[N];
		this.board = new char[N];
		for(int j = 0; j < board.cols(); j++) {
			for(int i = 0; i < board.rows(); i++) {
				this.board[j + i*board.cols()] = board.getLetter(i, j);
			}
		}
		for(int tile = 0; tile < N; tile++) {
			hasUp[tile] = (tile > cols - 1 );
			hasDown[tile] = (tile < N - cols);
			hasLeft[tile] = (tile % cols != 0);
			hasRight[tile] = tile % cols < cols - 1;
		}
	}

	private Iterable<Integer> adj(int tile){
		ArrayList<Integer> neighbors = new ArrayList<Integer>();

		if(hasUp[tile])	   neighbors.add(tile - cols);
		if(hasLeft[tile])  neighbors.add(tile - 1);
		if(hasDown[tile])  neighbors.add(tile + cols);
		if(hasRight[tile]) neighbors.add(tile + 1);   	
		
		if(hasLeft[tile]  && hasUp[tile])   neighbors.add(tile - cols - 1);
		if(hasLeft[tile]  && hasDown[tile]) neighbors.add(tile + cols - 1);
		if(hasRight[tile] && hasUp[tile])   neighbors.add(tile - cols + 1);
		if(hasRight[tile] && hasDown[tile]) neighbors.add(tile + cols + 1);
		
		return neighbors;
	}

//    private class results{
//    	public boolean isWord;
//    	public boolean isPrefix;
//    }
    
	private void search(int tile, boolean[] marked, SET<String> words) 
	{
		marked[tile] = true;

		prefix.append(board[tile]);
		if(board[tile] == 'Q') {
			//System.out.print("\n Detected: " + board[tile]);
			prefix.append('U');
		}

		String text = prefix.toString();
		boolean[] R = ST.search(text);
		boolean isWord = R[0];
		boolean isPrefix = R[1];
		if(isPrefix) {
			for (int i : adj(tile)) {
				if (!marked[i]) 
					search(i, marked, words);
			}
		}
		if(text.length() > 2) {
			if(isWord) words.add(text);
		}
		// if the last char is a U, we need to see if the prior char is a Q. If it is, remove that one too
		if(prefix.charAt(prefix.length()-1) == 'U' && prefix.length() > 1)
		{
				if(prefix.charAt(prefix.length()-2) == 'Q') {
					prefix.deleteCharAt(prefix.length()-1);

				}
		}
		prefix.deleteCharAt(prefix.length()-1);
		marked[tile] = false;
	}

	// Returns the set of all valid words in the given Boggle board, as an Iterable.
	public Iterable<String> getAllValidWords(BoggleBoard board){
		initilizeBoard(board);
		SET<String> words = new SET<String>();
		for(int i = 0; i < N; i++) {
			search(i, new boolean[N], words);
		}
		return words;
	}

	// Returns the score of the given word if it is in the dictionary, zero otherwise.
	// (You can assume the word contains only the uppercase letters A through Z.)
	public int scoreOf(String word) {
		if (!ST.search(word)[0]) return 0; //if not in dictionary, then not a word.
		ST.search(word);
		switch(word.length()) {
		case 0:
		case 1:
		case 2: return 0;
		case 3: 
		case 4: return 1;
		case 5: return 2;
		case 6: return 3;
		case 7: return 5;
		default: return 11;
		}
	}
	private void testScoring() {
//		String s = "";
//		System.out.println("testScoring:");
//		for (int i=0; i < 10; i++) {
//			System.out.printf("  The string %10s has a length %2d and score of : %02d \n", s, s.length(), scoreOf(s));
//			s += "A";
//		}
		
		List<String> words = Arrays.asList("COUNTRY", "CACHING", "STEEPLECHASE", "BLUSHEST", "FIREWOOD", "FIREWATER");
		for(String w : words) {
			System.out.printf("  The string %10s has a length %2d and score of : %02d \n", w, w.length(), scoreOf(w));
		}
	}
	private void printBoard() {
		for(int j = 0; j < this.cols; j++) {
			for(int i = 0; i < this.rows; i++)
			{
				System.out.print(board[j + i * cols]);
			}
			System.out.println();
		}
		System.out.println();
	}
	private void testJK() {
		System.out.println("testJK:");
		TrieJK<Boolean> t = new TrieJK<Boolean>();
		System.out.println("  Add some words.");
		List<String> words = Arrays.asList("BARN", "PRIVATE", "FIRE", "FIREFLY", "FIREWOOD", "FIREWATER");
		for(String s : words) {
			System.out.println("  Adding " + s);
			t.put(s);
		}
		System.out.println("  check some words.");
		for(String s : words) {
			System.out.println("  Does this now contain " + s + "? " + t.search(s)[0]);
			System.out.println("  Does *master* contain " + s + "? " + ST.search(s)[0]);
		}
		System.out.println("  check some fake words.");
		List<String> fakeWords = Arrays.asList("FROM", "FORM", "FINISH", "FIRST");
		for(String s : fakeWords) {
			System.out.println("  Does this now contain " + s + "? " + t.search(s)[0]);
			System.out.println("  Does *master* contain " + s + "? " + ST.search(s)[0]);
		}

	}
	
    private class Node {
        private boolean isWord;
        private boolean isPrefix;
        private Node[] next = new Node[26];
    }
    
	private class TrieJK<Value> {
	    private static final int R = 26;        // extended ASCII
	    private Node root = new Node();      // root of trie
	    private int n;          // number of keys in trie
	    // R-way trie node

	    public TrieJK() {
	    }
	    
	    public void put(String key) {
	        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
	        else root = put(root, key, 0);
	    }
	    private Node put(Node x, String key, int d) {
	        if (x == null) x = new Node();
	        if (d == key.length()) {
	        	// this means that all the char links have been traversed
	        	// which means that we have arrived out our destination
	        	// so, we can mark our current node as a valid word
	            if (x.isWord == false) n++;
	            x.isWord = true;
	            return x;
	        }
	        // if we got this far, we are still reading in the key
	        // we are descending down the tree
	        // we know that each node we pass will be a valid prefix,
	        // so we should mark it as such. 
	        // As soon as x.[i] is defined, 
	        // our current node is a prefix of something
	        x.isPrefix = true;
	        char c = key.charAt(d);
	        x.next[c -'A'] = put(x.next[c - 'A'], key, d+1);
	        return x;
	    }
	    
	    public boolean[] search(String key) {
	    	boolean[] results = new boolean[2];
	        if (key == null) throw new IllegalArgumentException("argument to get() is null");
	        Node x = get(root, key, 0);
	        if (x == null) {
	        	results[0] = false; //null is not a word
	        	results[1] = false; //null is also not a prefix
	        	return results;
	        }
	        results[0] = x.isWord;
	        results[1] = x.isPrefix;
	        return results;
	    }
	    private Node get(Node x, String key, int d) {
	        if (x == null) 
	        	return null; 
	        	// broke out of the trie
	        	// this means that key is not in trie
	        	// it also means that key is not a prefix in trie
	        if (d == key.length()) 
	        	return x;
	        	// this means that we have arrived at our desination
	        	// we can return the current node
	        	// we can assumed that all the chars have matched
	        	// the current node may or may not be a word or a prefix
	        	// we don't care! that information is stored in the node
	        	
	        char c = key.charAt(d);
	        Node result = get(x.next[c - 'A'], key, d+1);
	        // if this is null, that means that x.next[c - 'A'] is null
	        // which means that the word is not in the trie
	        // again, we don't have to test if the node is a word or a prefix
	        // whether it was a word or a prefix would have been marked on insertion.
	        return result;
	    }
	}

	public static void main(String[] args) {
		In in = new In(args[0]);
		//long start = System.currentTimeMillis();
		String[] dictionary = in.readAllStrings();
		BoggleSolver solver = new BoggleSolver(dictionary);
		//solver.testScoring();
		//solver.testJK();
		BoggleBoard board = new BoggleBoard(args[1]);
		int score = 0;
		int count = 0;
		//long startWords = System.currentTimeMillis();
		for (String word : solver.getAllValidWords(board)) {
			StdOut.println(word);
			score += solver.scoreOf(word);
			count++;
		}
		//long stop = System.currentTimeMillis();
		StdOut.println("Score = " + score);
		//System.out.println("Number of solutions: " + count);
		//System.out.println("Building the dictionary took " + (startWords-start) + "mS");
//		System.out.println("Searching the board took " + (stop - startWords) + "mS");
	}

}


